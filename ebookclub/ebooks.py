import os
import os.path
import zipfile
from xml.etree import ElementTree
import hashlib
import datetime

import isbnlib

from flask import (redirect, render_template, abort, url_for,
                   send_from_directory)
from werkzeug.utils import secure_filename
from flask_login import current_user, login_required
from sqlalchemy.orm.exc import NoResultFound

from forms import UploadForm, MetaDataForm, ReviewForm
from app import app
from models import Review, Ebook, Author, db


def get_epub_info_and_extract_cover(f):
    ns = {
        'n': 'urn:oasis:names:tc:opendocument:xmlns:container',
        'pkg': 'http://www.idpf.org/2007/opf',
        'dc': 'http://purl.org/dc/elements/1.1/'
    }

    z = zipfile.ZipFile(f)

    # locate OPF file
    txt = z.read('META-INF/container.xml')
    tree = ElementTree.fromstring(txt)
    cfname = tree.find('n:rootfiles/n:rootfile', namespaces=ns)

    if cfname is None:
        return

    cfname = cfname.attrib['full-path']

    # find metadata
    cf = z.read(cfname)
    tree = ElementTree.fromstring(cf)
    p = tree.find('./pkg:metadata', namespaces=ns)

    # repackage the data
    res = {}
    for s in ['title', 'language', 'creator', 'date', 'identifier']:
        r = p.find('dc:{}'.format(s), ns)
        if r is not None:
            res[s] = r.text

    m = tree.find('./pkg:manifest', namespaces=ns)

    for item in m:
        if item.attrib['id'] == 'cover-id':
            cover_url = item.attrib['href']
            cover = z.read('OEBPS/' + cover_url)
            with open(os.path.join(os.path.dirname(f), 'cover'), 'wb') as fh:
                fh.write(cover)
            break

    return res


@app.route('/upload', methods=['GET', 'POST'])
@login_required
def upload():
    form = UploadForm()
    if form.validate_on_submit():
        file_name = secure_filename(form.book_file.data.filename)

        file_data = form.book_file.data
        md5 = hashlib.md5(file_data.read()).hexdigest()
        full_path = os.path.join(app.config['UPLOAD_DIR'], md5, file_name)
        os.makedirs(os.path.dirname(full_path))
        file_data.seek(0)
        file_data.save(full_path)

        metadata = get_epub_info_and_extract_cover(full_path)

        # If identifier is a ISBN, this should work...
        isbn = ''
        if 'identifier' in metadata:
            try:
                isbn_res = isbnlib.meta(metadata['identifier'])
            except isbnlib._exceptions.NotValidISBNError:
                app.logger.warning('isbn not valid')
            else:
                isbn = metadata['identifier']
                print(isbn_res['Authors'])
                isbn_res['Authors'] = ",".join(isbn_res['Authors'])
                metadata.update(isbn_res)

        form = MetaDataForm()
        form.authors.data = metadata.get('Authors',
                                         metadata.get('creator', ''))
        form.title.data = metadata.get('Title',
                                       metadata.get('title',
                                                    ''))

        form.file_name.data = file_name
        form.isbn.data = isbn
        form.md5.data = md5

        return render_template('forms/metadata.html', form=form)
    return render_template('forms/upload.html', form=form)


@app.route('/metadata', methods=['POST'])
def metadata():
    form = MetaDataForm()
    if form.validate_on_submit():
        author_names = form.authors.data.lower().split(",")
        authors = []
        for author_name in author_names:
            a = Author.query.filter_by(name=author_name).first()
            if a is not None:
                authors.append(a)
            else:
                new_author = Author(name=author_name)
                db.session.add(new_author)
                authors.append(new_author)

        ebook = Ebook(title=form.title.data,
                      summary=form.summary.data,
                      authors=authors,
                      isbn=form.isbn.data,
                      uploader=current_user,
                      md5=form.md5.data,
                      file_name=form.file_name.data,
                      date_added=datetime.date.today())
        db.session.add(ebook)

        review = Review(ebook=ebook,
                        content=form.review.data,
                        reviewer=current_user,
                        date_modified=datetime.date.today(),
                        date_submitted=datetime.date.today())
        db.session.add(review)

        db.session.commit()

        return redirect('/ebook/{}'.format(ebook.id))

    return redirect('/')


@app.route('/ebooks')
def ebooks():
    ebooks = Ebook.query.all()
    return render_template("pages/ebooks.html", ebooks=ebooks)


@app.route('/ebook/<int:ebook_id>')
def ebook_details(ebook_id):
    try:
        ebook = Ebook.query.filter_by(id=ebook_id).one()
    except NoResultFound:
        abort(404)
    else:
        form = ReviewForm()
        form.ebook_id.data = ebook.id
        current_user_review = Review.query.filter_by(
            ebook=ebook,
            reviewer=current_user).first()
        if current_user_review is not None:
            form.content.data = current_user_review.content
            form.review_id.data = current_user_review.id
        return render_template("pages/ebook.html",
                               ebook=ebook,
                               form=form)


@app.route('/review', methods=['POST'])
def review():
    form = ReviewForm()
    if form.validate_on_submit():
        if form.review_id.data:
            review = Review.query.filter_by(id=form.review_id.data).one()
            review.date_modified = datetime.date.today()
            review.content = form.content.data
        else:
            ebook = Ebook.query.filter_by(id=form.ebook_id.data).one()
            review = Review(ebook=ebook, reviewer=current_user,
                            date_submitted=datetime.date.today(),
                            content=form.content.data)
            db.session.add(review)
        db.session.commit()
    return redirect(url_for('ebook_details', ebook_id=form.ebook_id.data))


@app.route('/upload/<path:path>')
def serve_uploaded_file(path):
    """This should be overridden by nginx, just here for debug"""
    return send_from_directory(app.config['UPLOAD_DIR'], path)
