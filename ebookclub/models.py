from flask import url_for
from flask_sqlalchemy import SQLAlchemy
from app import app

from flask_login import UserMixin

db = SQLAlchemy(app)

# users_ebooks = db.Table('users_ebooks', db.metadata,
#                         db.Column('user_id', db.Integer,
#                                   db.ForeignKey('users.id')),
#                         db.Column('ebook_id', db.Integer,
#                                   db.ForeignKey('ebooks.id')))

authors_ebooks = db.Table('authors_ebooks', db.metadata,
                          db.Column('author_id', db.Integer,
                                    db.ForeignKey('authors.id')),
                          db.Column('ebook_id', db.Integer,
                                    db.ForeignKey('ebooks.id')))


class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    jabber_id = db.Column(db.String(120), unique=True, nullable=False)
    uploaded_ebooks = db.relationship("Ebook", back_populates="uploader")
    reviews = db.relationship("Review", back_populates="reviewer")

    def __init__(self, jabber_id=None):
        self.jabber_id = jabber_id

    def __repr__(self):
        return "<User '" + self.jabber_id + "'>"

    def __str__(self):
        return self.jabber_id.split("@")[0]

    def get_id(self):
        return self.jabber_id


class Author(db.Model):
    __tablename__ = 'authors'

    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(128))

    ebooks = db.relationship("Ebook",
                             secondary=authors_ebooks,
                             back_populates='authors')

    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return "<Author '" + self.name + "'>"

    def __str__(self):
        return self.name.title()


class Ebook(db.Model):
    __tablename__ = 'ebooks'

    id = db.Column(db.Integer, primary_key=True)

    uploader_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    uploader = db.relationship("User", back_populates="uploaded_ebooks")

    authors = db.relationship("Author",
                              secondary=authors_ebooks,
                              back_populates='ebooks')

    title = db.Column(db.String(1024))

    isbn = db.Column(db.String(13))

    md5 = db.Column(db.String(32))

    reviews = db.relationship("Review", back_populates="ebook")

    summary = db.Column(db.Text)

    file_name = db.Column(db.String(128))

    date_added = db.Column(db.Date)

    def __init__(self,
                 title=None,
                 authors=None,
                 isbn=None,
                 uploader=None,
                 md5=None,
                 file_name=None,
                 summary=None,
                 date_added=None):
        self.title = title
        self.authors = authors
        self.isbn = isbn
        self.uploader = uploader
        self.md5 = md5
        self.file_name = file_name
        self.summary = summary
        self.date_added = date_added

    def __repr__(self):
        return "<Ebook '" + self.title + "' (" + self.md5 + ")>"

    @property
    def file(self):
        return "/".join((app.config['UPLOAD_DIR_EXTERNAL'],
                         self.md5,
                         self.file_name))

    @property
    def cover_url(self):
        return app.config['UPLOAD_DIR_EXTERNAL'] + '/' + self.md5 + '/cover'

    @property
    def url(self):
        return url_for('ebook_details', ebook_id=self.id)

    @property
    def reviewers(self):
        return [r.reviewer for r in self.reviews]


class Review(db.Model):
    __tablename__ = 'reviews'

    id = db.Column(db.Integer, primary_key=True)

    reviewer_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    reviewer = db.relationship("User", back_populates="reviews")

    ebook_id = db.Column(db.Integer, db.ForeignKey('ebooks.id'))
    ebook = db.relationship("Ebook", back_populates="reviews")

    content = db.Column(db.String(10240))

    date_submitted = db.Column(db.Date)
    date_modified = db.Column(db.Date)

    def __init__(self, ebook=None, content=None, reviewer=None,
                 date_submitted=None, date_modified=None):
        self.reviewer = reviewer
        self.ebook = ebook
        self.content = content
        self.date_modified = date_modified
        self.date_submitted = date_submitted

    def __repr__(self):
        return "<Review on '{}' by '{}'>".format(self.ebook,
                                                 self.reviewer)


@app.cli.command()
def initdb():
    db.drop_all()
    db.create_all()
    db.session.commit()
    print("All good.")
