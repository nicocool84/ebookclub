from urllib.request import (build_opener,
                            HTTPPasswordMgrWithDefaultRealm,
                            HTTPBasicAuthHandler,
                            HTTPError)
from urllib.parse import urlparse, urljoin

from flask_login import LoginManager, login_user, login_required, logout_user
from flask import render_template, request, redirect, flash, url_for, abort

from app import app
from models import db, User
from forms import LoginForm

login_manager = LoginManager()
login_manager.init_app(app)


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc


@login_manager.user_loader
def load_user(jabber_id):
    return User.query.filter_by(jabber_id=jabber_id).first()


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        jabber_id = request.form['jabber_id']
        password = request.form['password']
        password_mgr = HTTPPasswordMgrWithDefaultRealm()
        password_mgr.add_password(None,
                                  uri=app.config['PROSODY_AUTH_CHECK'],
                                  user=jabber_id,
                                  passwd=password)
        handler = HTTPBasicAuthHandler(password_mgr)
        opener = build_opener(handler)
        try:
            opener.open(app.config['PROSODY_AUTH_CHECK'])
        except HTTPError as e:
            flash("Bad login: {}".format(e))
        else:
            flash("Good login")
            jabber_id = request.form['jabber_id']
            user = User.query.filter_by(jabber_id=jabber_id).first()
            next_page = request.args.get('next')
            if not is_safe_url(next_page):
                return abort(400)
            if user is None:
                app.logger.info(
                    "User {} correctly authenticated, but no "
                    "entry in DB, creating it.".format(jabber_id))
                user = User(jabber_id=jabber_id)
                db.session.add(user)
                db.session.commit()
            login_user(load_user(jabber_id))
            return redirect(next_page or url_for('home'))

    return render_template('forms/login.html', form=form)


@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))


login_manager.login_view = "login"
