#!/usr/bin/env python

from flask import render_template, abort
from flask_login import login_required

from app import app
from models import User


@app.route("/users/")
@login_required
def users():
    return render_template("pages/users.html", users=User.query.all())


@app.route("/user/<int:user_id>")
@login_required
def user_details(user_id):
    user = User.query.filter_by(id=user_id).first()
    if user is None:
        abort(404)
    return render_template("pages/user.html",
                           user=user)
