from flask_wtf import FlaskForm
from wtforms import (StringField, PasswordField, FileField, TextAreaField,
                     HiddenField)
from wtforms.validators import DataRequired


class LoginForm(FlaskForm):
    jabber_id = StringField('Jabber ID', [DataRequired()])
    password = PasswordField('Password', [DataRequired()])


class UploadForm(FlaskForm):
    book_file = FileField('File to upload', [DataRequired()])


class MetaDataForm(FlaskForm):
    authors = StringField('Authors', [DataRequired()])
    title = StringField('Title', [DataRequired()])
    summary = TextAreaField('Résumé')
    review = TextAreaField('Avis', [DataRequired()])
    md5 = HiddenField('md5', [DataRequired()])
    file_name = HiddenField('file_name', [DataRequired()])
    isbn = HiddenField('isbn')


class ReviewForm(FlaskForm):
    content = TextAreaField('Avis')
    ebook_id = HiddenField()
    review_id = HiddenField()
