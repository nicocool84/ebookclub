#!/usr/bin/env python

import os

basedir = os.path.abspath(os.path.dirname(__file__))

SECRET_KEY = 'verysecret1337passw0rd'

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'database.db')
SQLALCHEMY_TRACK_MODIFICATIONS = True

PROSODY_AUTH_CHECK = 'http://localhost:5280/auth_check'

UPLOAD_DIR = os.path.join(basedir, 'upload')
UPLOAD_DIR_EXTERNAL = os.path.join('/upload')
